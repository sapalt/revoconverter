//
//  SymbolsProvider.swift
//  revoconverter
//
//  Created by Alexander Shchavrovskiy on 18/11/2018.
//  Copyright © 2018 Alexander Shchavrovskiy. All rights reserved.
//

import Foundation
import Alamofire

protocol SymbolsProvider {
    func tryToGetSymbols()
    var symbols: Symbols? { get }
}

struct Symbols: Decodable {
    let symbols: [Currency: String]
}

final class DefaultSymbolsProvider: SymbolsProvider {
    
    private(set) var symbols: Symbols?
    
    func tryToGetSymbols() {
        Alamofire.request("http://data.fixer.io/api/symbols?access_key=bf857b16170caa90c2b348c063fc8616").responseJSON { [weak self] response in
            self?.symbols = response.data.flatMap { try? JSONDecoder().decode(Symbols.self, from: $0) }
        }
    }
}
