//
//  RatesProvider.swift
//  revoconverter
//
//  Created by Alexander Shchavrovskiy on 18/11/2018.
//  Copyright © 2018 Alexander Shchavrovskiy. All rights reserved.
//

import Foundation
import Alamofire

typealias Currency = String

struct Rates: Decodable, Equatable {
    let base: Currency
    let rates: [Currency: Double]
}

protocol RatesProviderListener: class {
    func ratesDidUpdate()
}

protocol RatesProvider {
    func set(listener: RatesProviderListener)
    func start()
    func setBaseCurrency(base: Currency)
    func stop()
    var currentRates: Rates? { get }
    var lastKnownRates: Rates? { get }
}

final class DefaultRatesProvider: RatesProvider {
    
    private(set) var currentRates: Rates?
    private(set) var lastKnownRates: Rates?
    
    private weak var listener: RatesProviderListener?

    init(base: String) {
        self.base = base
    }
    
    private var base: String = "EUR"
    
    private var lastRequestStartTime: CFAbsoluteTime = CFAbsoluteTimeGetCurrent()
    private var timer: Timer?
    private var lastRequest: DataRequest?

    func set(listener: RatesProviderListener) {
        self.listener = listener
    }
    
    func start() {
        timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { [weak self] _ in
            self?.makeRequest()
        }
    }
    
    func setBaseCurrency(base: Currency) {
        lastRequest?.cancel()
        self.base = base
        timer?.fire()
    }
    
    private func makeRequest() {
        lastRequest = Alamofire.request("https://revolut.duckdns.org/latest?base=\(base)").responseJSON { [weak self] response in
            guard let slf = self else { return }
            guard response.timeline.requestStartTime > slf.lastRequestStartTime else { return }
            
            slf.lastRequestStartTime = response.timeline.requestStartTime
            
            guard let rates = response.data.flatMap ({ try? JSONDecoder().decode(Rates.self, from: $0) }) else {
                slf.currentRates = nil
                return
            }
            
            if rates == slf.currentRates {
                return
            }
            
            slf.currentRates = rates
            slf.lastKnownRates = rates
            slf.listener?.ratesDidUpdate()
        }
    }
    
    func stop() {
        timer?.invalidate()
        timer = nil
    }
}

extension Rates {
    func amount(for currency: Currency, baseAmount: Double) -> Double {
        guard let rate = rates[currency] else {
            assert(false)
            return 0
        }
        return rate * baseAmount
        
    }
}
