//
//  AppDelegate.swift
//  revoconverter
//
//  Created by Alexander Shchavrovskiy on 16/11/2018.
//  Copyright © 2018 Alexander Shchavrovskiy. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    private var bootstrapper: Bootstrapper?
    
    func application(
        _ application: UIApplication,
        didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool
    {
        let window = UIWindow(frame: UIScreen.main.bounds)
        self.window = window

        self.bootstrapper = Bootstrapper(window: window)
        bootstrapper?.start()

        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        bootstrapper?.pause()
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        bootstrapper?.resume()
    }
    
}

