//
//  ConverterViewController.swift
//  revoconverter
//
//  Created by Alexander Shchavrovskiy on 16/11/2018.
//  Copyright © 2018 Alexander Shchavrovskiy. All rights reserved.
//

import UIKit

final class ConverterViewController: UIViewController {

    init(model: ConverterViewModel) {
        self.viewModel = model

        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()

        viewModel.set(delegate: self)
    }

    private let viewModel: ConverterViewModel
    private let tableView = UITableView(frame: .zero, style: .grouped)
    
    private var pendingAction: (() -> Void)?

    private func setupUI() {

        title = Strings.converterTitle

        view.backgroundColor = .white

        apply(tableView) {
            $0.delegate = self
            $0.dataSource = self

            $0.register(CurrencyCell.self, forCellReuseIdentifier: CurrencyCell.cellReuseIdentifier)

            $0.keyboardDismissMode = .onDrag

            view.addSubview($0)

            $0.backgroundColor = .white

            $0.frame = view.bounds
            $0.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        }
    }
}

extension ConverterViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        viewModel.didSelectRow(at: indexPath)
    }
    
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        pendingAction?()
        pendingAction = nil
    }
}

extension ConverterViewController: UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.infos.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(
            withIdentifier: CurrencyCell.cellReuseIdentifier, for: indexPath) as? CurrencyCell else
        {
            assert(false)
            return UITableViewCell()
        }

        cell.update(with: viewModel.infos[indexPath.row])
        

        return cell
    }
}

extension ConverterViewController: ConverterViewModelDelegate {
    func viewModelDidUpdate() {
        let isFirst = tableView.cellForRow(at: .init(row: 0, section: 0))?.isFirstResponder ?? false
        tableView.reloadData()
        if isFirst {
            UIView.performWithoutAnimation { [weak self] in
                self?.tableView.cellForRow(at: .init(row: 0, section: 0))?.becomeFirstResponder()
            }

        }
    }
    
    func upCell(indexPath: IndexPath, completion: (() -> Void)?) {

        tableView.performBatchUpdates({ [weak self] in
            self?.tableView.moveRow(at: indexPath, to: .init(row: 0, section: 0))
            }, completion: { [weak self] _ in
                guard let slf = self else { return }
                let neededContentOffset = -slf.tableView.adjustedContentInset.top
                if neededContentOffset != slf.tableView.contentOffset.y {
                    self?.tableView.setContentOffset(.init(x: 0, y: neededContentOffset), animated: true)
                    self?.pendingAction = completion
                } else {
                    completion?()
                }
                
        })
    }
    
    func startEdit() {
        tableView.cellForRow(at: .init(row: 0, section: 0))?.becomeFirstResponder()
    }
}

