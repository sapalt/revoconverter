//
//  ConverterViewModel.swift
//  revoconverter
//
//  Created by Alexander Shchavrovskiy on 16/11/2018.
//  Copyright © 2018 Alexander Shchavrovskiy. All rights reserved.
//

import Foundation

protocol ConverterViewModelDelegate: class {
    func viewModelDidUpdate()
    func upCell(indexPath: IndexPath, completion: (() -> Void)?)
    func startEdit()
}

protocol ConverterViewModel {
    func didSelectRow(at indexPath: IndexPath)
    func set(delegate: ConverterViewModelDelegate)
    var infos: [CurrencyCell.Info] { get }
}

final class DefaultConverterViewModel: ConverterViewModel {

    init(model: ConverterModel, baseText: String?) {
        self.baseText = baseText
        self.model = model
        model.set(delegate: self)
    }

    func set(delegate: ConverterViewModelDelegate) {
        self.delegate = delegate
    }
    
    func didSelectRow(at indexPath: IndexPath) {
        guard indexPath.row != 0 else {
            delegate?.startEdit()
            return
        }

        state = .notReadyToUpdate

        guard model.setBase(atIndex: indexPath.row - 1) else {
            assert(false)
            return
        }
        
        let info = infos.remove(at: indexPath.row)
        let amount = DefaultConverterViewModel.textToAmount(info.amount)

        baseText = info.amount
        model.change(amount: amount)

        infos.insert(info, at: 0)
        
        delegate?.upCell(indexPath: indexPath) { [weak self] in
            self?.state = .readyToUpdate
            self?.delegate?.startEdit()
        }
    }

    private enum State {
        case readyToUpdate
        case notReadyToUpdate
    }

    private var state: State = .readyToUpdate

    private let model: ConverterModel

    private(set) var infos: [CurrencyCell.Info] = []

    private static func amountToText(_ amount: Double) -> String? {
        if amount == 0 {
            return nil
        } else {
            return numberFormatter.string(from: NSNumber(value: amount))
        }
    }

    private static func textToAmount(_ text: String?) -> Double {
        guard let text = text else { return 0 }
        if let number = numberFormatter.number(from: text) {
            return number.doubleValue
        } else {
            assert(false)
            return 0
        }
    }

    private func setNeedsUpdate() {

        infos = model.infos.map { info in
            let amount = DefaultConverterViewModel.amountToText(info.amount)

            return .init(icon: nil, shortName: info.currency, fullName: info.symbol, amount: amount) { [weak self] str in
                self?.onChangeText(withText: str)
            }
        }

        let baseInfo = CurrencyCell.Info(
            icon: nil, shortName: model.baseInfo.currency,
            fullName: model.baseInfo.symbol, amount: baseText)
        { [weak self] str in
            self?.onChangeText(withText: str)
        }

        infos.insert(baseInfo, at: 0)

        delegate?.viewModelDidUpdate()
    }

    private func onChangeText(withText text: String?) {
        let separator = Locale.current.decimalSeparator ?? "."
        let val = text == "0" ? nil : (text == separator ? "0\(separator)" : text)
        baseText = val
        let amount = DefaultConverterViewModel.textToAmount(val)
        model.change(amount: amount)
    }

    private var baseText: String?

    private weak var delegate: ConverterViewModelDelegate?

    private static let numberFormatter: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.roundingMode = .halfUp
        formatter.maximumFractionDigits = 3
        return formatter
    }()
}

extension DefaultConverterViewModel: ConverterModelDelegate {
    func modelDidUpdate() {
        guard state != .notReadyToUpdate else { return }
        setNeedsUpdate()
    }
}
