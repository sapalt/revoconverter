//
//  ConverterModel.swift
//  revoconverter
//
//  Created by Alexander Shchavrovskiy on 23/11/2018.
//  Copyright © 2018 Alexander Shchavrovskiy. All rights reserved.
//

import Foundation

struct ModelInfo {
    let currency: Currency
    let symbol: String
    let amount: Double
}

protocol ConverterModelDelegate: class {
    func modelDidUpdate()
}

protocol ConverterModel {
    func setBase(atIndex index: Int) -> Bool
    func change(amount: Double)
    func set(delegate: ConverterModelDelegate)
    var baseInfo: ModelInfo { get }
    var infos: [ModelInfo] { get }
}

final class DefaultConverterModel: ConverterModel {

    init(rates: RatesProvider, symbols: SymbolsProvider, baseAmount: Double, baseCurrency: Currency) {
        self.baseAmount = baseAmount
        self.rates = rates
        self.symbols = symbols
        self.baseInfo = ModelInfo.init(currency: baseCurrency, symbol: "", amount: baseAmount)
        
        rates.set(listener: self)
        rates.start()
        symbols.tryToGetSymbols()
    }

    func setBase(atIndex index: Int) -> Bool {
        guard index < infos.count else { return false }
        let info = infos.remove(at: index)
        rates.setBaseCurrency(base: info.currency)
        infos.insert(baseInfo, at: 0)
        baseInfo = info
        return true
    }

    func change(amount: Double) {
        baseAmount = amount
        updateInfos()
    }

    func set(delegate: ConverterModelDelegate) {
        self.delegate = delegate
    }

    private weak var delegate: ConverterModelDelegate?

    private(set) var baseInfo: ModelInfo
    private(set) var infos: [ModelInfo] = []

    private func updateInfos() {
        if let r = rates.currentRates {

            infos = r.rates.map { key, value in
                let amount = r.amount(for: key, baseAmount: baseAmount)
                return ModelInfo(
                    currency: key,
                    symbol: symbols.symbols?.symbols[key] ?? "",
                    amount: amount)
            }

            baseInfo = ModelInfo(
                currency: r.base, symbol: symbols.symbols?.symbols[r.base] ?? "",
                amount: baseAmount)

            delegate?.modelDidUpdate()
        }
    }

    private var baseAmount: Double

    private let rates: RatesProvider
    private let symbols: SymbolsProvider
}

extension DefaultConverterModel: RatesProviderListener {

    func ratesDidUpdate() {
        updateInfos()
    }
}
