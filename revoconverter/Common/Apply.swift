//
//  Apply.swift
//  revoconverter
//
//  Created by Alexander Shchavrovskiy on 16/11/2018.
//  Copyright © 2018 Alexander Shchavrovskiy. All rights reserved.
//

import Foundation

@discardableResult
public func apply<T>(_ obj: T, _ block: (T) -> Void) -> T where T: AnyObject {
    block(obj)
    return obj
}
