//
//  Bootstrapper.swift
//  revoconverter
//
//  Created by Alexander Shchavrovskiy on 02/12/2018.
//  Copyright © 2018 Alexander Shchavrovskiy. All rights reserved.
//

import UIKit

final class Bootstrapper {

    init(window: UIWindow) {
        self.window = window

        rates = DefaultRatesProvider(base: "EUR")
        symbols = DefaultSymbolsProvider()
    }

    private let rates: RatesProvider
    private let symbols: SymbolsProvider

    private weak var window: UIWindow?

    func start() {
        let vc = makeRootViewController()
        let navigationController = makeNavigationController(with: vc)

        window?.rootViewController = navigationController
        window?.makeKeyAndVisible()
    }

    func pause() {
        rates.stop()
    }

    func resume() {
        rates.start()
        symbols.tryToGetSymbols()
    }
}

extension Bootstrapper {
    
    private func makeRootViewController() -> UIViewController {

        let model = DefaultConverterModel(rates: rates, symbols: symbols, baseAmount: 1, baseCurrency: "EUR")
        let viewModel = DefaultConverterViewModel(model: model, baseText: "1")
        return ConverterViewController(model: viewModel)
    }

    private func makeNavigationController(with root: UIViewController) -> UINavigationController {
        guard let window = window else {
            assert(false)
            return UINavigationController()
        }

        let navigationController = UINavigationController(rootViewController: root)
        navigationController.view.frame = window.bounds
        navigationController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        navigationController.navigationBar.prefersLargeTitles = true
        return navigationController
    }
}
