 //  Copyright © 2018 Alexander Shchavrovskiy. All rights reserved.
 //


 import UIKit
 

 final class CurrencyCell: UITableViewCell {

    static var cellReuseIdentifier = "CurrencyCell"

    struct Info {
        let icon: UIImage?
        let shortName: String
        let fullName: String
        var amount: String?
        let onChangeText: ((String?) -> Void)?
    }

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func update(with info: Info) {
        iconView.image = info.icon
        shortName.text = info.shortName
        fullName.text = info.fullName
        moneyAmount.text = info.amount
        onChangeText = info.onChangeText

        amountLine.isHidden = info.amount == nil
    }

    private struct Static {
        static let iconSize = CGSize(width: 40, height: 40)

        static let space: CGFloat = 16
        static let namesSpace: CGFloat = 4

        static let lineHeight: CGFloat = 6
    }
    
    override func becomeFirstResponder() -> Bool {
        return moneyAmount.becomeFirstResponder()
    }
    
    override var isFirstResponder: Bool {
        return moneyAmount.isFirstResponder
    }
    
    private var onChangeText: ((String?) -> Void)?

    private let iconView = UIImageView()
    private let shortName = UILabel()
    private let fullName = UILabel()
    private let moneyAmount = TouchPassTextField()
    private let amountLine = UIView()

    private func setup() {
        setupSubviews()
        setupConstraints()
    }

    private func setupSubviews() {
        apply(iconView) {
            $0.layer.cornerRadius = Static.iconSize.height / 2
            $0.layer.masksToBounds = true
            $0.layer.borderWidth = 2
            $0.layer.borderColor = UIColor.black.cgColor
            contentView.addSubview($0)
        }

        apply(shortName) {
            $0.textColor = Colors.majorText
            $0.font = Fonts.major
            $0.numberOfLines = 1
            contentView.addSubview($0)
        }

        apply(fullName) {
            $0.textColor = Colors.minorText
            $0.font = Fonts.minor
            $0.numberOfLines = 2
            contentView.addSubview($0)
        }

        apply(moneyAmount) {
            $0.font = Fonts.big
            $0.textColor = Colors.majorText
            $0.placeholder = "0"
            $0.keyboardType = .decimalPad
            $0.delegate = self
            $0.addTarget(self, action: #selector(handleChangeText), for: .editingChanged)
            contentView.addSubview($0)
        }

        apply(amountLine) {
            $0.backgroundColor = Colors.line
            $0.layer.cornerRadius = Static.lineHeight / 2
            $0.layer.masksToBounds = true
            contentView.addSubview($0)


        }
    }
    
    @objc private func handleChangeText() {
        if let text = moneyAmount.text, text.isEmpty {
            onChangeText?(nil)
        } else {
            onChangeText?(moneyAmount.text)
        }
    }

    private func setupConstraints() {
        apply(iconView) {
            $0.translatesAutoresizingMaskIntoConstraints = false
            $0.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: Static.space).isActive = true
            $0.topAnchor.constraint(equalTo: contentView.topAnchor, constant: Static.space).isActive = true
            $0.heightAnchor.constraint(equalToConstant: Static.iconSize.height).isActive = true
            $0.widthAnchor.constraint(equalToConstant: Static.iconSize.width).isActive = true
        }

        apply(shortName) {
            $0.translatesAutoresizingMaskIntoConstraints = false
            $0.topAnchor.constraint(equalTo: iconView.topAnchor).isActive = true
            $0.leftAnchor.constraint(equalTo: iconView.rightAnchor, constant: Static.space).isActive = true
            $0.setContentCompressionResistancePriority(.fittingSizeLevel, for: .horizontal)
        }

        apply(fullName) {
            $0.translatesAutoresizingMaskIntoConstraints = false
            $0.topAnchor.constraint(equalTo: shortName.bottomAnchor, constant: Static.namesSpace).isActive = true
            $0.leftAnchor.constraint(equalTo: shortName.leftAnchor).isActive = true
            $0.setContentCompressionResistancePriority(.fittingSizeLevel, for: .horizontal)
            $0.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -Static.space).isActive = true
        }

        apply(moneyAmount) {
            $0.translatesAutoresizingMaskIntoConstraints = false
            $0.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -Static.space).isActive = true
            $0.centerYAnchor.constraint(equalTo: contentView.centerYAnchor).isActive = true
            $0.topAnchor.constraint(greaterThanOrEqualTo: contentView.topAnchor, constant: Static.space).isActive = true
            $0.bottomAnchor.constraint(lessThanOrEqualTo: contentView.bottomAnchor, constant: -Static.space).isActive = true
            $0.leftAnchor.constraint(equalTo: fullName.rightAnchor, constant: Static.space).isActive = true
            $0.leftAnchor.constraint(equalTo: shortName.rightAnchor, constant: Static.space).isActive = true
            $0.setContentCompressionResistancePriority(.defaultHigh, for: .horizontal)
            $0.setContentHuggingPriority(.defaultHigh, for: .horizontal)
        }

        apply(amountLine) {
            $0.translatesAutoresizingMaskIntoConstraints = false
            $0.topAnchor.constraint(equalTo: moneyAmount.bottomAnchor).isActive = true
            $0.leadingAnchor.constraint(equalTo: moneyAmount.leadingAnchor).isActive = true
            $0.trailingAnchor.constraint(equalTo: moneyAmount.trailingAnchor).isActive = true
            $0.heightAnchor.constraint(equalToConstant: Static.lineHeight).isActive = true
        }
    }
 }

 extension CurrencyCell: UITextFieldDelegate {
    func textField(
        _ textField: UITextField,
        shouldChangeCharactersIn range: NSRange,
        replacementString string: String) -> Bool
    {
        guard let text = textField.text else { return false }

        guard let range = Range(range, in: text) else {
            assert(false)
            return false
        }

        let newText = text.replacingCharacters(in: range, with: string)

        if newText.isEmpty {
            return true
        }

        return newText.count - newText.filter(("0"..."9").contains).count <= 1 && newText.count < 10
    }
 }

 private final class TouchPassTextField: UITextField {
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        return nil
    }
 }
