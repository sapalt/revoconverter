//
//  Colors.swift
//  revoconverter
//
//  Created by Alexander Shchavrovskiy on 16/11/2018.
//  Copyright © 2018 Alexander Shchavrovskiy. All rights reserved.
//

import UIKit

enum Colors {
    static let majorText: UIColor = .black
    static let minorText: UIColor = .gray

    static let line = UIColor.blue.withAlphaComponent(0.4)
}
