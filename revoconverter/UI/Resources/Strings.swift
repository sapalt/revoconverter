//
//  Strings.swift
//  revoconverter
//
//  Created by Alexander Shchavrovskiy on 02/12/2018.
//  Copyright © 2018 Alexander Shchavrovskiy. All rights reserved.
//

import Foundation

enum Strings {
    static let converterTitle = NSLocalizedString(
        "converter.title", value: "Converter", comment: "Title for Converter VC")
}

