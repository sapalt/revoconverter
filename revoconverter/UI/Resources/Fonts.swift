//
//  Fonts.swift
//  revoconverter
//
//  Created by Alexander Shchavrovskiy on 16/11/2018.
//  Copyright © 2018 Alexander Shchavrovskiy. All rights reserved.
//

import UIKit

enum Fonts {
    static let major = UIFont.systemFont(ofSize: UIFont.systemFontSize)
    static let minor = UIFont.systemFont(ofSize: UIFont.smallSystemFontSize)
    static let big = UIFont.systemFont(ofSize: UIFont.systemFontSize * 2)
}
