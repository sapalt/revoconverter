//
//  DefaultConverterViewTests.swift
//  revoconverterTests
//
//  Created by Alexander Shchavrovskiy on 28/11/2018.
//  Copyright © 2018 Alexander Shchavrovskiy. All rights reserved.
//

import XCTest
@testable import revoconverter

final class DefaultConverterViewTest: XCTestCase {

    func testInfoParsing() {
        let mock = MockDataModel()
        let model = DefaultConverterViewModel(model: mock, baseText: "baseText")
        mock.set(delegate: model)
        mock.delegate?.modelDidUpdate()

        XCTAssert(model.infos.count == mock.infos.count + 1)
        XCTAssert(model.infos[0] == CurrencyCell.Info.init(icon: nil, shortName: "0", fullName: "", amount: "baseText", onChangeText: nil))
        XCTAssert(model.infos[1] == CurrencyCell.Info.init(icon: nil, shortName: "1", fullName: "", amount: "1", onChangeText: nil))
        XCTAssert(model.infos[2] == CurrencyCell.Info.init(icon: nil, shortName: "2", fullName: "", amount: "2", onChangeText: nil))
        XCTAssert(model.infos[5] == CurrencyCell.Info.init(icon: nil, shortName: "5", fullName: "", amount: nil, onChangeText: nil))

        let separator = Locale.current.decimalSeparator
        XCTAssertNotNil(separator)
        if let sep = separator {
            XCTAssert(model.infos[3] == CurrencyCell.Info.init(icon: nil, shortName: "3", fullName: "sym3", amount: "2\(sep)5", onChangeText: nil))
            XCTAssert(model.infos[4] == CurrencyCell.Info.init(icon: nil, shortName: "4", fullName: "", amount: "3\(sep)123", onChangeText: nil))
        }
    }

    func testInfosAfterChangeBase() {
        let mock = MockDataModel()
        let model = DefaultConverterViewModel(model: mock, baseText: "baseText")
        mock.set(delegate: model)
        mock.delegate?.modelDidUpdate()

        mock.setBase(atIndex: 4)
        mock.delegate?.modelDidUpdate()

        XCTAssert(model.infos.count == mock.infos.count + 1)
        XCTAssert(model.infos[0] == CurrencyCell.Info.init(icon: nil, shortName: "5", fullName: "", amount: "baseText", onChangeText: nil))
        XCTAssert(model.infos[1] == CurrencyCell.Info.init(icon: nil, shortName: "0", fullName: "", amount: nil, onChangeText: nil))
        XCTAssert(model.infos[2] == CurrencyCell.Info.init(icon: nil, shortName: "1", fullName: "", amount: "1", onChangeText: nil))
        XCTAssert(model.infos[3] == CurrencyCell.Info.init(icon: nil, shortName: "2", fullName: "", amount: "2", onChangeText: nil))
        let separator = Locale.current.decimalSeparator
        XCTAssertNotNil(separator)
        if let sep = separator {
            XCTAssert(model.infos[4] == CurrencyCell.Info.init(icon: nil, shortName: "3", fullName: "sym3", amount: "2\(sep)5", onChangeText: nil))
            XCTAssert(model.infos[5] == CurrencyCell.Info.init(icon: nil, shortName: "4", fullName: "", amount: "3\(sep)123", onChangeText: nil))
        }
    }

    func testInfosAfterChangeBaseViaSelectingRow() {
        let mock = MockDataModel()
        let model = DefaultConverterViewModel(model: mock, baseText: "baseText")
        mock.set(delegate: model)
        mock.delegate?.modelDidUpdate()

        let delegate = MockDelegate()
        model.set(delegate: delegate)
        model.didSelectRow(at: .init(row: 5, section: 0))
        mock.delegate?.modelDidUpdate()

        XCTAssert(model.infos.count == mock.infos.count + 1)
        XCTAssert(model.infos[0] == CurrencyCell.Info.init(icon: nil, shortName: "5", fullName: "", amount: nil, onChangeText: nil))
        XCTAssert(model.infos[1] == CurrencyCell.Info.init(icon: nil, shortName: "0", fullName: "", amount: nil, onChangeText: nil))
        XCTAssert(model.infos[2] == CurrencyCell.Info.init(icon: nil, shortName: "1", fullName: "", amount: "1", onChangeText: nil))
        XCTAssert(model.infos[3] == CurrencyCell.Info.init(icon: nil, shortName: "2", fullName: "", amount: "2", onChangeText: nil))

        let separator = Locale.current.decimalSeparator
        XCTAssertNotNil(separator)
        if let sep = separator {
            XCTAssert(model.infos[4] == CurrencyCell.Info.init(icon: nil, shortName: "3", fullName: "sym3", amount: "2\(sep)5", onChangeText: nil))
            XCTAssert(model.infos[5] == CurrencyCell.Info.init(icon: nil, shortName: "4", fullName: "", amount: "3\(sep)123", onChangeText: nil))
        }
    }
}


private func ==(lhs: CurrencyCell.Info, rhs: CurrencyCell.Info) -> Bool {
    return lhs.amount == rhs.amount && lhs.fullName == rhs.fullName && lhs.shortName == rhs.shortName
}

private final class MockDelegate: ConverterViewModelDelegate {
    func viewModelDidUpdate() {}

    func upCell(indexPath: IndexPath, completion: (() -> Void)?) {
        completion?()
    }

    func startEdit() {}
}


private final class MockDataModel: ConverterModel {

    @discardableResult
    func setBase(atIndex index: Int) -> Bool {
        guard infos.indices ~= index else { return false }
        let info = infos.remove(at: index)
        infos.insert(baseInfo, at: 0)
        baseInfo = info
        return true
    }

    func change(amount: Double) {}

    func set(delegate: ConverterModelDelegate) {
        self.delegate = delegate
    }

    weak var delegate: ConverterModelDelegate?

    var baseInfo: ModelInfo = ModelInfo(currency: "0", symbol: "", amount: 0)

    var infos: [ModelInfo] = [
        ModelInfo(currency: "1", symbol: "", amount: 1),
        ModelInfo(currency: "2", symbol: "", amount: 2.0),
        ModelInfo(currency: "3", symbol: "sym3", amount: 2.5),
        ModelInfo(currency: "4", symbol: "", amount: 3.123456),
        ModelInfo(currency: "5", symbol: "", amount: 0)
    ]
}
