//
//  ConverterViewControllerSnapshotTests.swift
//  revoconverterTests
//
//  Created by Alexander Shchavrovskiy on 07/12/2018.
//  Copyright © 2018 Alexander Shchavrovskiy. All rights reserved.
//

import XCTest
import FBSnapshotTestCase
@testable import revoconverter

final class ConverterViewControllerSnapshotTests: FBSnapshotTestCase {

    override func setUp() {
        super.setUp()
        recordMode = false
    }

    func testNormal() {
        let model = MockedViewModel()
        let vc = ConverterViewController.init(model: model)

        FBSnapshotVerifyView(vc.view)
    }
}

// MARK: - Private

private final class MockedViewModel: ConverterViewModel {
    func didSelectRow(at indexPath: IndexPath) {}

    func set(delegate: ConverterViewModelDelegate) {}

    var infos: [CurrencyCell.Info] = [
        .init(icon: nil, shortName: "EUR", fullName: "Euro", amount: "1,22", onChangeText: nil),
        .init(icon: nil, shortName: "EUR EUR EUR EUR EUR EUR",
              fullName: "Euro long long long long long long long long long",
              amount: "1,2223492789472", onChangeText: nil),
        .init(icon: nil, shortName: "", fullName: "", amount: "", onChangeText: nil),
        .init(icon: nil, shortName: "EUR EUR EUR EUR EUR EUR",
              fullName: "Euro long long long long long long long long long",
              amount: "1,0", onChangeText: nil),
    ]
}


