//
//  DefaultConverterModelTests.swift
//  revoconverterTests
//
//  Created by Alexander Shchavrovskiy on 28/11/2018.
//  Copyright © 2018 Alexander Shchavrovskiy. All rights reserved.
//

import XCTest
@testable import revoconverter

final class DefaultConverterModelTests: XCTestCase {

    func testInfoMaking() {
        let rates = MockRatesProvider()
        let symbols = MockSymbolsProvider()

        let model = DefaultConverterModel(rates: rates, symbols: symbols, baseAmount: 2, baseCurrency: "EUR")
        rates.set(listener: model)
        rates.listener?.ratesDidUpdate()

        XCTAssert(model.infos.count == rates.rates.rates.count)
        XCTAssert(model.baseInfo == ModelInfo(currency: "EUR", symbol: "euro", amount: 2))
        XCTAssert(model.infos.filter { $0 == ModelInfo(currency: "USD", symbol: "", amount: 2 * 1.4) }.count == 1)
        XCTAssert(model.infos.filter { $0 == ModelInfo(currency: "RUB", symbol: "", amount: 2 * 71) }.count == 1)
    }
}

private func ==(lhs: ModelInfo, rhs: ModelInfo) -> Bool {
    return lhs.symbol == rhs.symbol && lhs.amount == rhs.amount && lhs.currency == rhs.currency
}

private final class MockRatesProvider: RatesProvider {

    func set(listener: RatesProviderListener) {
        self.listener = listener
    }

    weak var listener: RatesProviderListener?

    func start() {}

    func setBaseCurrency(base: Currency) {}

    func stop() {}

    var rates: Rates = Rates(base: "EUR", rates: ["USD": 1.4, "RUB": 71])

    var currentRates: Rates? {
        return self.rates
    }

    var lastKnownRates: Rates?


}

private final class MockSymbolsProvider: SymbolsProvider {

    var symbols: Symbols? = Symbols(symbols: ["EUR": "euro"])

    func tryToGetSymbols() {}
}
